# Overview

Build the LFS system. This is the third step in LFS pipeline and covers chapters 
[6](http://www.linuxfromscratch.org/lfs/view/9.1-systemd/chapter06/introduction.html), 
[7](http://www.linuxfromscratch.org/lfs/view/9.1-systemd/chapter07/introduction.html) and 
[8](http://www.linuxfromscratch.org/lfs/view/9.1-systemd/chapter08/introduction.html) of the LFS book.

Refer to the `docs` project for a general overview of the whole `automated-linux-from-scratch` group.

# Build

Execute `docker build` and overwrite build arguments when applicable. 

For example:
```shell script
$ export TOOLCHAIN_REGISTRY_IMAGE=toolchain
$ export TOOLCHAIN_TAG=dev
$ export CPUS=4
$ export ROOT_PASS=my-custom-password
$ export SKIP_TAGS='test'
$ docker build -t lfs-system:dev -f Dockerfile \
    --build-arg TOOLCHAIN_REGISTRY_IMAGE \
    --build-arg TOOLCHAIN_TAG \
    --build-arg CPUS \
    --build-arg ROOT_PASS \
    --build-arg SKIP_TAGS \
    ansible
```

will have the following effects:
- local `toolchain:dev` image used as a builder image,
- `4 CPUs` used during compilation,
- `my-custom-password` set as a root user password,
- `test` tags skipped, i.e. package testing will not occur,

and produce an image `lfs-system:dev`.
