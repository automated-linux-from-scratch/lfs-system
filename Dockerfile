ARG TOOLCHAIN_REGISTRY_IMAGE=registry.gitlab.com/automated-linux-from-scratch/toolchain
ARG TOOLCHAIN_TAG=master
FROM ${TOOLCHAIN_REGISTRY_IMAGE}:${TOOLCHAIN_TAG} as builder
ARG CPUS=1
ARG ROOT_PASSWORD=strongest_password
ARG SKIP_TAGS=''
ENV ANSIBLE_CONFIG=/ansible/ansible.cfg
ENV ANSIBLE_FORCE_COLOR=1
ENV NPROC=${CPUS}
ENV ROOT_PASS=${ROOT_PASSWORD}
ENV SKIP_TAGS=${SKIP_TAGS}
# ensure ansible can be executed
RUN ["/tools/bin/bash", "-c", "echo root:x:0:0:root:/root:/bin/bash > /etc/passwd"]
RUN ["/tools/bin/bash", "-c", "echo root:x:0: > /etc/group"]
RUN ["/tools/bin/bash", "-c", "mkdir /bin"]
RUN ["/tools/bin/bash", "-c", "ln -s /tools/bin/bash /bin/sh"]
COPY . /ansible
RUN ["/tools/bin/bash", "-c", "ansible-playbook /ansible/10_install_basic_software.yml --diff --skip-tags \"${SKIP_TAGS}\" -e 'ansible_python_interpreter=/tools/bin/python3 ansible_shell_executable=/tools/bin/bash'"]
RUN ["/bin/bash", "-c", "ansible-playbook /ansible/11_install_basic_software_with_compiled_bash.yml --diff --skip-tags \"${SKIP_TAGS}\" -e 'ansible_python_interpreter=/tools/bin/python3 ansible_shell_executable=/bin/bash'"]
RUN ["/bin/bash", "-c", "ansible-playbook /ansible/12_install_basic_software_with_compiled_python.yml --diff --skip-tags \"${SKIP_TAGS}\" -e 'ansible_python_interpreter=/usr/bin/python3 ansible_shell_executable=/bin/bash'"]
RUN ["/bin/bash", "-c", "ansible-playbook /ansible/20_make_system_bootable.yml --diff -e 'ansible_python_interpreter=/usr/bin/python3 ansible_shell_executable=/bin/bash'"]

FROM scratch as lfs-system
LABEL maintainer="Aleksander Ciołek <aleksander.ciolek@protonmail.com>"
COPY --from=builder /bin /bin/
COPY --from=builder /boot /boot/
COPY --from=builder /lib /lib/
COPY --from=builder /lib64 /lib64/
COPY --from=builder /media /media/
COPY --from=builder /usr /usr/
COPY --from=builder /etc /etc/
COPY --from=builder /sbin /sbin/
CMD ["/bin/bash"]
